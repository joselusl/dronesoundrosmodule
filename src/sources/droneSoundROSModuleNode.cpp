//////////////////////////////////////////////////////
//  DroneSoundROSModuleNode.cpp
//
//  Created on: Jul 3, 2013
//      Author: joselusl
//
//  Last modification on: Oct 23, 2013
//      Author: joselusl
//
//////////////////////////////////////////////////////



//I/O Stream
//std::cout
#include <iostream>



// ROS
//ros::init(), ros::NodeHandle, ros::ok(), ros::spinOnce()
#include "ros/ros.h"




#include "droneSoundROSModule.h"

#include "nodes_definition.h"


using namespace std;


int main(int argc, char **argv)
{
    ros::init(argc, argv, MODULE_NAME_DRONE_SOUND_ROS_MODULE);

    ros::NodeHandle nh;


    DroneSoundROSModule MyDroneSoundROSModule;
    MyDroneSoundROSModule.open(nh);


    ros::spin();


    return 0;
}

